---
author: Fabien CAYRE
---

# Advanced Text Algorithm - Spring 2022 - Assignment 1

## Fabien CAYRE

---

#### 1. Construct both the table of borders and the table of strong borders for the pattern "maamamma".

Table of borders:

| value | Bord | StrongBord |
| ----- | ---- | ---------- |
| 0     | -1   | -1         |
| 1     | 0    | 0          |
| 2     | 0    | 0          |
| 3     | 0    | -1         |
| 4     | 1    | 0          |
| 5     | 2    | 2          |
| 6     | 1    | 1          |
| 7     | 1    | 0          |
| 8     | 2    | 2          |

#### 2. Use the tables computed in 1 to trace (dry run) the MP algorithm for the text “jo_hommaamme_maamamman”

```shell
~: python borders.py
maamamma
...

does "jo_hommaamme_maamamman" contains "maamamma" ? Yes
```

#### 3. Use the tables computed in 1 to trace (dry run) the KMP algorithm for the text “jo_hommaamme_maamamman”

#### 4. Given a word w of length m, we define the function R(x) for all characters x in the alphabet as follows: R(x)=0 if the character x does not occur in w and R(x)=max{i<m | w[i]=x} otherwise. Provide an algorithm that computes the function R(x) in O(m) time.

```
# Worst case scenario = O(m)
# Best case scenario = O(1)
procedure R(x)
  i := m
  while i > 0 do begin
    if w[i] = x then
      return(i)
  end while
  return(0)
end
```


#### 5. Trace (dry run) the BM algorithm to locate occurrences of pattern “maamamma” in text “jo_hommaamme_maamamman” by

##### a. applying the bad character shift rule alone

##### b. applying the good suffix shift rule alone

##### c. selecting the maximum shift given by the bad character rule and the good suffix rule

(Note that in the lecture slides for the case of the bad character rule the mismatch is between
pat[i] and text[i+j], while in the case of the good suffix rule the mismatch is between pat[i-1]
and text[i+j-1])
