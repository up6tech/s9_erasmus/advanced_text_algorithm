from func import r, table_L, table_l
    
def bad_character_rule(text: str, pat: str, j: int = -1) -> int:
  n = len(text)
  m = len(pat)
  if j == -1:
    j = 0
  i = 0

  occurences = []

  L = table_L(pat, m)
  l = table_l(pat, m)

  k = m
  while k < n:
    i = m
    h = k
    while i > 0 and pat[i-1] == text[h-1]:
      i = i - 1
      h = h - 1
    if i == 0:
      occurences.append(k)
      print(f"occurence at {k}")
      print(f"k = k + m - l[1]; k = {k} + {m} - {l[1]};k = {k + m - l[1]}")
      k = k + m - l[1]
    else:
      # print(f"k = {k} + max(1, {i} - r({text[h-1]}, {pat}, {m}));k = {k} + max(1, {i}-{r(text[h-1], pat, m)})")
      print(f"k = k + max(1, i - R(text[h-1]);k = {k} + {max(1, i - r(text[h-1], pat, m))}")        
      k = k + max(1, i - r(text[h-1], pat, m))
      # if i == m:
      #   print(f"k = k + 1; k = {k} + 1")
      #   k = k + 1
      # else:
      #   print(f"k = k + max(1, i - R(text[h-1]); k = {k} + {max(1, i - r(text[h-1], pat, m))}")        
      #   k = k + max(1, i - r(text[h-1], pat, m))
  return occurences


def good_suffix_rule(text: str, pat: str, j: int = -1) -> int:
  n = len(text)
  m = len(pat)
  if j == -1:
    j = 0
  i = 0

  occurences = []

  L = table_L(pat, m)
  l = table_l(pat, m)

  k = m
  while k < n:
    i = m
    h = k
    while i > 0 and pat[i-1] == text[h-1]:
      i = i - 1
      h = h - 1
    if i == 0:
      occurences.append(k)
      print(f"occurence at {k}")
      print(f"k = k + m - l[1]; k = {k} + {m} - {l[1]}; k = {k + m - l[1]}")
      k = k + m - l[1]
    else:
      if i == m:
        print(f"k = k + 1; k = {k} + 1")
        k = k + 1
      elif L[i+1] == 0:
        print(f"k = k + max(m - l(i+1), 1); k = {k} + {max(m - l[i+1], 1)}")
        k = k + max(m - l[i+1], 1)
      else:
        print(f"k = k + max(m - L(i+1), 1); k = {k} + {max(m - L[i+1], 1)}")
        k = k + max(m - L[i+1], 1)
  return occurences

def max_shift(text: str, pat: str, j: int = -1) -> int:
  n = len(text)
  m = len(pat)
  if j == -1:
    j = 0
  i = 0

  occurences = []

  L = table_L(pat, m)
  l = table_l(pat, m)

  k = m
  while k < n:
    i = m
    h = k
    while i > 0 and pat[i-1] == text[h-1]:
      i = i - 1
      h = h - 1
    if i == 0:
      occurences.append(k)
      print(f"occurence at {k}")
      print(f"k = k + m - l[1]; k = {k} + {m} - {l[1]}; k = {k + m - l[1]}")
      k = k + m - l[1]
    else:
      if i == m:
        print(f"k = k + max(1, i - R(text[h-1]); k = {k} + max(1,{i}-{r(text[h-1], pat, m)})")
        k = k + max(1, i-r(text[h-1], pat, m))
      elif L[i+1] == 0:
        print(f"k = k + max(m - l(i+1), 1, i - R(text[h-1]); k = {k} + {max(m - l[i+1], 1, i - r(text[h-1], pat, m))}")
        k = k + max(m - l[i+1], 1, i - r(text[h-1], pat, m))
      else:
        print(f"k = k + max(m - L(i+1), 1, i - R(text[h-1]); k = {k} + {max(m - L[i+1], 1, i - r(text[h-1], pat, m))}")
        k = k + max(m - L[i+1], 1, i - r(text[h-1], pat, m))

  return occurences

