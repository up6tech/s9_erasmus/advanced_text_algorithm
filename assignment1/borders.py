from mp import mp
# Use list to mutate the string

def borders(pat: str) -> list[int]:
  pat = list(pat)
  m = len(pat)
  borders = {}

  def bord(n: int):
    return "".join(pat[0:n])

  t = -1
  borders[bord(0)] = -1

  for j in range(1, m+1):
    while t >= 0 and pat[t] != pat[j-1]:
      t = borders[bord(t)]
    t = t+1
    borders[bord(j)] = t

  res = "".join(pat)
  borders_value = list(borders.values())
  print(f"Bord={borders_value}")
  return borders_value

def strong_borders(pat: str) -> list[int]:
  pat = list(pat)
  m = len(pat)
  borders = {}

  def strong_bord(n: int):
    return "".join(pat[0:n])

  t = -1
  borders[strong_bord(0)] = -1

  for j in range(1, m+1):
    while t >= 0 and pat[t] != pat[j-1]:
      t = borders[strong_bord(t)]
    t = t+1
    if j == m or pat[t] != pat[j]:
      borders[strong_bord(j)] = t
    else:
      borders[strong_bord(j)] = borders[strong_bord(t)]

  res = "".join(pat)
  borders_value = list(borders.values())
  print(f"Strong_Bord={borders_value}")
  return borders_value
  

