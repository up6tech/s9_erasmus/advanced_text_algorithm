# Given a word w of length m, we define the function R(x) for all characters x in the alphabet
# as follows: R(x)=0 if the character x does not occur in w and R(x)=max{i<m | w[i]=x}
# otherwise. Provide an algorithm that computes the function R(x) in O(m) time.

# Worst case scenario = O(m)
# Best case scenario = O(1)
# for 
def r(x: str, w: str, m: int) -> int:
  for index in reversed(range(0, m)):
    if w[index] == x:
      return index
  return 0

def Naive_Scan(pat: str, m: int, p,q) -> int:
    result = 0
    while p < m and q < m:
      if pat[p] != pat[q]:
        break
      p = p + 1
      q = q + 1
      result = result + 1
    return result

def Compute_Prefixes(pat: str, m: int) -> list[int]:
  PREF = [0]*m
  PREF[0] = -1
  s = 0
  for j in range (1, m):
    k = j - s + 1
    r = s + PREF[s] - 1
    if r < j:
      PREF[j] = Naive_Scan(pat,m, j - 1, 0)
      if PREF[j] > 0:
        s = j
    elif PREF[k]+k < PREF[s]:
      PREF[j] = PREF[k]
    else:
      x = Naive_Scan(pat,m, r, r - j + 1)
      PREF[j] = r - j + 1 + x 
      s = j
  PREF[0] = m
  return PREF

  
def table_L(pat: str, m: int):
  prefixes = Compute_Prefixes(pat[::-1], m)

  N = lambda j: prefixes[m-j-1]

  L = [-1]*(m+1)
  for i in range(1, m+1):
    L[i] = 0
  for j in range(0, m-1):
    i = m - N(j)
    L[i] = j
  return L

def table_l(pat: str, m: int):
  prefixes = Compute_Prefixes(pat[::-1], m)

  N = lambda j: prefixes[m-j-1]

  L = [-1]*(m+1)
  for i in range(1, m+1):
    L[i] = 0
  for j in range(0, m-1):
    i = N(j)
    L[i] = j
  return L

def r_all(w: str, m: int):
  alphabet = "abcdefghijklmnopqrstuvwxyz"
  for index in range(0, len(alphabet)):
    print(r(alphabet[index], w, m))