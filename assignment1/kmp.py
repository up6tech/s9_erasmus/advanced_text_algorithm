def kmp(pat: str, text: str, Strong_Bord: list[int]) -> tuple[list[tuple[int]], bool]:
  i = 0
  j = 0
  n = len(text)
  m = len(pat)

  var = []
  KMP_Shift = lambda j: j - Strong_Bord[j]

  step = 1
  j2 = -888
  while i <= n-m:
    while j < m and pat[j] == text[i+j]:
      j = j+1
    if j == m: return (var, True)
    prev_i = i
    prev_j = j

    i = i + KMP_Shift(j)
    j = max(0, j -KMP_Shift(j))
    var.append((i, Strong_Bord[j]))

    print(f"{step})")
    print(f"\ti = i + KMP_Shift(j); i = {prev_i} + {KMP_Shift(prev_j)}; i = {i}")
    if j != j2:
      j2 = j
      print(f"\tj = max(0, j - KMP_Shift(j)); j = max(0, {(prev_j - KMP_Shift(prev_j))}); j = {j}")
    step+=1
  return (var, False)