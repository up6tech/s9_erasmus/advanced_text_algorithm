def mp(pat: str, text: str, Bord: list[int]) -> tuple[list[tuple[int]], bool]:
  i = 0
  j = 0
  n = len(text)
  m = len(pat)

  var = []
  MP_Shift = lambda j: j - Bord[j]

  step = 1
  j2 = -888
  while i <= n-m:
    while j < m and pat[j] == text[i+j]:
      j = j+1
    if j == m: return (var, True)
    prev_i = i
    prev_j = j

    i = i + MP_Shift(j)
    j = max(0, j -MP_Shift(j))
    
      
    print(f"{step})")
    print(f"\ti = i + MP_Shift(j); i = {prev_i} + {MP_Shift(prev_j)}; i = {i}")
    if j != j2:
      j2 = j
      print(f"\tj = max(0, j - MP_Shift(j)); j = max(0, {(prev_j - MP_Shift(prev_j))}); j = {j}")
    step+=1
  return (var, False)