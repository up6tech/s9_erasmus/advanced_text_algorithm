from borders import borders, strong_borders
from kmp import kmp
from mp import mp
from bm import bad_character_rule, good_suffix_rule, max_shift

pattern = "maamamma"
text = "jo_hommaamme_maamamman"

# text = "babababaababaabababababb"
# pattern = "abababababb"
print(f"Python search string return index {text.index(pattern)}")
print(f"len of pattern {len(pattern)}")
index = bad_character_rule(text, pattern)
print(f"Bad character rule implementation return index {index}")
print("===============================")
index = good_suffix_rule(text, pattern)
print(f"Good suffix rule implementation return index {index}")
print("===============================")
index = max_shift(text, pattern)
print(f"Max shift implementation return index {index}")

print(pattern)
print("Preprocessing Bord and Strong_Bord ...")
Bord = borders(pattern)
StrongBord = strong_borders(pattern)
trace, contains_pattern = mp(pattern, text, Bord)

result = "Yes" if contains_pattern else "No"
print("====== Morris-Pratt ======")
print(trace)
print(f"does \"{text}\" contains \"{pattern}\" ? {result}")
print("================================")

trace, contains_pattern = kmp(pattern, text, StrongBord)
print("====== Knuth-Morris-Pratt ======")
print(trace)
print(f"does \"{text}\" contains \"{pattern}\" ? {result}")